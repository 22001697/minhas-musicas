//
//  DetalheMusicaViewController.swift
//  Minhas musicas
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit

class DetalheMusicaViewController: UIViewController {

    var nomeImagem: String = ""
    var nomeDaMusica: String = ""
    var nomeAlbum: String = ""
    var nomeCantor:String = ""
    
    @IBOutlet weak var capa: UIImageView!
    @IBOutlet weak var Musica: UILabel!
    @IBOutlet weak var Album: UILabel!
    @IBOutlet weak var Cantor: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.capa.image = UIImage(named: self.nomeImagem)
        self.Musica.text = self.nomeDaMusica
        self.Album.text = self.nomeAlbum
        self.Cantor.text = self.nomeCantor
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
